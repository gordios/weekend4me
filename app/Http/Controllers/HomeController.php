<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        /*if(Auth::check()) {
            $user = Auth::user();
            if($user->hasRole('admin')) {
                return redirect(route('admin.dashboard.index'));
            }
        } else {
             return view('client.dashboard.index');
        }*/

        return view('client.dashboard.index');
    }
}
