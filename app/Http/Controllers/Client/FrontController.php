<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

class FrontController extends Controller
{


    public function place()
    {
      return view('front.pages.place');
    }

    public function flights()
    {
      return view('front.pages.flights');
    }

    public function news()
    {
      return view('front.pages.news');
    }

    public function about()
    {
      return view('front.pages.about');
    }

    public function contact()
    {
      return view('front.pages.contact');
    }



}
