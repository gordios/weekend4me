<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function flight() {
        return $this->hasMany('App/Models/Flight');
    }
}
