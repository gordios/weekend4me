<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    public function airplane() {
        return $this->hasOne('App/Models/Airplane');
    }

    public function airport() {
        return $this->hasOne('App/Models/Airport');
    }

    public function country() {
        return $this->hasOne('App/Models/Country');
    }
}
