<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;
use App\Models\Role;

class MakeFirstUserAsAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role_admin = Role::where('name','admin')->first();

        User::create([
            'name'=> 'test',
            'email'=>'test@gmail.com',
            'password'=>'$2y$10$ptv35lok243y1xJRL7uKiejfQgSR3G95bVkT/E95i2U0sXmQWjF4G',
            'remember_token'=>'BJw5saXDSp5bytCkULzpXYrULZT4mPhyIOA9HjlTuMHhu0kfykotc7EgbX8t']);

        User::first()->roles()->attach($role_admin->id);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
