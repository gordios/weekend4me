let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 /*
 ========================================
 ADMIN CSS AND JS
 ========================================
 */
//Admin styles CSS
mix.styles([
  'resources/assets/admin/css/bootstrap.min.css',
  'resources/assets/admin/font-awesome/css/font-awesome.css',
  'resources/assets/admin/css/animate.css',
  'resources/assets/admin/css/style.css',
], 'public/css/admin.css').sourceMaps();

//Admin scripts JS
mix.scripts([
  //MAIN
  'resources/assets/admin/js/jquery-3.1.1.min.js',
  'resources/assets/admin/js/bootstrap.min.js',
  'resources/assets/admin/js/plugins/metisMenu/jquery.metisMenu.js',
  'resources/assets/admin/js/plugins/slimscroll/jquery.slimscroll.min.js',
  //FLOT
  'resources/assets/admin/js/plugins/flot/jquery.flot.js',
  'resources/assets/admin/js/plugins/flot/jquery.flot.tooltip.min.js',
  'resources/assets/admin/js/plugins/flot/jquery.flot.spline.js',
  'resources/assets/admin/js/plugins/flot/jquery.flot.resize.js',
  'resources/assets/admin/js/plugins/flot/jquery.flot.pie.js',
  'resources/assets/admin/js/plugins/flot/jquery.flot.symbol.js',
  'resources/assets/admin/js/plugins/flot/jquery.flot.time.js',
  //PEITY
  'resources/assets/admin/js/plugins/peity/jquery.peity.min.js',
  'resources/assets/admin/js/demo/peity-demo.js',
  //CUSTOM AND PLUGIN JAVASCRIPT
  'resources/assets/admin/js/inspinia.js',
  'resources/assets/admin/js/plugins/pace/pace.min.js',
  //jQuery UI
  'resources/assets/admin/js/plugins/jquery-ui/jquery-ui.min.js',
  //Jvectormap
  'resources/assets/admin/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js',
  'resources/assets/admin/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
  //EayPIE
  'resources/assets/admin/js/plugins/easypiechart/jquery.easypiechart.js',
  //Sparkline
  'resources/assets/admin/js/plugins/sparkline/jquery.sparkline.min.js',
  'resources/assets/admin/js/demo/sparkline-demo.js'

], 'public/js/admin/admin.js').sourceMaps();


//Admin custom JS
mix.js('resources/assets/js/admin/admin-custom.js', 'public/js/admin');

//Admin image
mix.copy('resources/assets/admin/img', 'public/img/admin');

/*
========================================
FLIGHT CSS AND JS
========================================
*/
//Flight styles CSS
mix.styles([
  'resources/assets/flight/css/jquery-ui.structure.min.css',
  'resources/assets/flight/css/jquery-ui.theme.min.css',
  'resources/assets/flight/css/bootstrap.min.css',
  'resources/assets/flight/css/custom.css',
  'resources/assets/flight/css/flatpickr.min.css',
  'resources/assets/flight/css/font-awesome.min.css',
  'resources/assets/flight/css/fontawesome-stars-o.css',
  'resources/assets/flight/css/swiper.min.css',
  'resources/assets/flight/css/jquery.mCustomScrollbar.min.css',
  'resources/assets/flight/css/select2.min.css',
  'resources/assets/flight/css/nouislider.min.css',
  'resources/assets/flight/css/nice-select.css',
  'resources/assets/flight/css/blueimp-gallery.min.css',
  'resources/assets/flight/style.min.css',
  'resources/assets/flight/skins/style-default.css',
], 'public/css/flight.css').sourceMaps();

//Flight scripts JS
mix.scripts([
  'resources/assets/flight/js/maps-key.js',
  'resources/assets/flight/js/maps.js',
  'resources/assets/flight/js/libs/moment.min.js',
  'resources/assets/flight/js/libs/wNumb.js',
  'resources/assets/flight/js/libs/nouislider.min.js',
  'resources/assets/flight/js/libs/jquery.min.js',
  'resources/assets/flight/js/libs/jquery-ui.min.js',
  'resources/assets/flight/js/libs/barba.min.js',
  'resources/assets/flight/js/libs/sticky-kit.min.js',
  'resources/assets/flight/js/libs/velocity.min.js',
  'resources/assets/flight/js/libs/jquery.waypoints.min.js',
  'resources/assets/flight/js/libs/popper.min.js',
  'resources/assets/flight/js/libs/bootstrap.min.js',
  'resources/assets/flight/js/libs/imagesloaded.pkgd.min.js',
  'resources/assets/flight/js/libs/masonry.pkgd.min.js',
  'resources/assets/flight/js/libs/isotope.pkgd.min.js',
  'resources/assets/flight/js/libs/ofi.min.js',
  'resources/assets/flight/js/libs/jarallax.min.js',
  'resources/assets/flight/js/libs/jarallax-video.min.js',
  'resources/assets/flight/js/libs/jarallax-element.min.js',
  'resources/assets/flight/js/libs/jquery.mCustomScrollbar.min.js',
  'resources/assets/flight/js/libs/jquery.nice-select.min.js',
  'resources/assets/flight/js/libs/swiper.min.js',
  'resources/assets/flight/js/libs/flatpickr/flatpickr.min.js',
  'resources/assets/flight/js/libs/flatpickr/rangePlugin.js',
  'resources/assets/flight/js/libs/select2.min.js',
  'resources/assets/flight/js/libs/select2/en.js',
  'resources/assets/flight/js/libs/jquery.mask.min.js',
  'resources/assets/flight/js/libs/validator.min.js',
  'resources/assets/flight/js/libs/jquery.barrating.min.js',
  'resources/assets/flight/js/libs/jquery.blueimp-gallery.min.js',
  'resources/assets/flight/js/script.min.js',
  'resources/assets/flight/js/demo-switcher.js',
], 'public/js/flight/flight.js').sourceMaps();

//Flight custom JS
mix.js('resources/assets/js/flight/flight-custom.js', 'public/js/flight');

//Flight image
mix.copy('resources/assets/flight/img', 'public/img/flight');



//Upload image
mix.copy('resources/assets/uploads/', 'public/uploads');

//FontAwesome fonts
mix.copy('resources/assets/flight/fonts', 'public/fonts');
mix.copy('resources/assets/flight/fonts', 'public/css/fonts');



/*
========================================
ALL CUSTOM CSS AND JS
========================================
*/
mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
