<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');

Route::group([
    'namespace' => 'Client',
    'as' => 'client.'
], function () {
    Route::get('place-example', 'PlaceController@example')->name('place.example');
    Route::resource('deal', 'DealController')->only('index');
    Route::resource('place', 'PlaceController')->only('index');
    Route::resource('flight', 'FlightController')->only('index');
    Route::resource('news', 'NewsController')->only('index');
    Route::resource('about', 'AboutController')->only('index');
    Route::resource('contact', 'ContactController')->only('index');
});

Route::group([
    'middleware' => ['auth','admin'],
    'namespace' => 'Admin',
    'prefix' => 'admin',
    'as' => 'admin.'
], function () {
    Route::resource('dashboard', 'DashboardController')->only('index');
});


