(($)=> {
    $("#searchDepartFlightDate,#searchReturnFlightDate").flatpickr({
        dateFormat: "Y-m-d",
        defaultDate: [new Date],
        onValueUpdate: function(selectedDates, dateStr, instance) {
            let day = moment(dateStr).format('dddd');
            let dayString =  String(day);
            if (dayString == 'Tuesday' || dayString == 'Wednesday' || dayString == 'Thursday' ) {
                alert('Flight not available from Tuesday to thursday.');
                $("#searchDepartFlightDate,#searchReturnFlightDate").val('');
            }
        }
    });
})(window.jQuery);