(($) => {
    let $travelDayBtn = $('[data-travel-day-btn]');
    let $travelDaysmodal = $('#travelDaysModal');
    let $airportModal = $('#airportModal');
    let $passengersModal = $('#passengersModal');
    let $inputSearchDepartDate = $('[data-search-input-depart-date]');
    let $inputSearchDepartDateString = $('[data-search-depart-day-string]');

    $travelDayBtn.on('click', function() {
        //manage day exception
        if(checkAlertException(this)) {
            return false;
        }

        let btnDateIndex = $(this).attr('data-travel-day-index');
        let btnDate = moment().startOf('week').add('days', btnDateIndex).format("YYYY-MM-DD");
        //update hidden date input value in form to submit
        updateFieldValue($inputSearchDepartDate, btnDate);
        //update searchbar day string on travel day select
        let day = moment(btnDate).format('dddd');
        let dayString = String(day);
        $inputSearchDepartDateString.text(dayString);
        //hide modal
        $travelDaysmodal.modal('hide');

    });

    let checkAlertException = ($element) => {
        if($element.hasAttribute('travel-alert-day-btn')) {
            alert('Flight not available from Tuesday to thursday.');
            $travelDaysmodal.modal('hide');
            return true;
        }
        return false;
    };

    let updateFieldValue = ($field, $value) => {
        $field.val($value);
    };

})(window.jQuery);