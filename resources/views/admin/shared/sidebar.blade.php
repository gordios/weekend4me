<div class="sidebar-collapse">
    <ul class="nav metismenu" id="side-menu">
        <li class="nav-header">
            <div class="dropdown profile-element">
                <span>
                    <img alt="image" class="img-circle" src="{{asset('/img/admin/profile_small.jpg')}}" />
                 </span>
                <span class="clear">
                    <span class="block m-t-xs">
                        <strong class="font-bold">{{Auth::user()->name}}</strong>
                    </span>
                </span>
            </div>
            <div class="logo-element">
                WK+
            </div>
        </li>
        <li class="active">
            <a href="{{route('admin.dashboard.index')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
        </li>

        <li>
            <a href="#"><i class="fa fa-diamond"></i> <span class="nav-label">Booking</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                 <li><a href="#">Flight</a></li>
                 <li><a href="#">Hotel</a></li>
             </ul>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-sitemap"></i>
                <span class="nav-label">Profile</span>
            </a>
        </li>
        <li>
          <a href="{{ route('logout') }}"
              onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
          <i class="fa fa-magic"></i>  <span class="nav-label">Logout</span>
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
          </form>
        </li>
    </ul>
</div>
