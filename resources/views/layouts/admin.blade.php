<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Weekend4Me</title>
    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/admin.css') }}">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>

<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        @include('admin.shared.sidebar')
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            @include('admin.shared.header')
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            @include('admin.shared.top-navbar')
        </div>
        <div class="wrapper wrapper-content">
            @yield('content')
            <div class="footer">
                @include('admin.shared.footer')
            </div>
        </div>
        <div id="right-sidebar">
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ mix('js/admin/admin.js') }}"></script>
<script src="{{ mix('js/admin/admin-custom.js') }}"></script>
@stack('scripts')

</body>
</html>
