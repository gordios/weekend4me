<div class="intro__search">
    <div class="container">
        <div class="search-hotels shadow-sm">
            <div class="tab-content">
                <div class="tab-pane active show" id="tabHotel" role="tabpanel">
                    <form class="search-hotels__form" action="#" method="GET" data-toggle="validator">
                        <div class="row">

                            <div class="form-group col-12 col-md-6 col-lg-4 col-xl-3">
                                <label class="label-text">Departure Airport</label>
                                <div class="d-flex" data-toggle="modal" data-target="#airportModal" style="cursor:pointer;">
                                    <i class="mr-2 icon icon-airplane text-secondary"></i>
                                    From any Airport
                                </div>
                            </div>

                            <div class="form-group col-12 col-md-6 col-lg-4 col-xl-4 d-flex justify-content-center">
                                <div class="form-group-date text-nowrap text-center">
                                    <div class="d-inline-block" data-toggle="modal" data-target="#travelDaysModal" style="cursor:pointer;">
                                        <label class="label-text">Travels Days</label>
                                        <div class="input-date-group position-relative">
                                            <i class="mr-2 icon icon-calendar text-secondary"></i>
                                        <span data-search-depart-day-string>Saturday</span> - Sunday
                                            {{Form::hidden('q[depart_date]', null, ['data-search-input-depart-date'])}}
                                            <label class="form-control date" for="hotelDate1"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-12 col-lg-4 col-xl-3 d-flex justify-content-center">
                                <div class="mx-2">
                                    <label class="label-text">Travellers</label>
                                    <div class="qty" data-toggle="modal" data-target="#passengersModal" style="cursor:pointer;">
                                        <i class="mr-2 icon icon-calendar text-secondary"></i>
                                        Two-Adults
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-12 col-xl-2 d-flex">
                                <button class="btn btn-secondary btn--round align-self-center" type="submit">Search
                                </button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <!-- Modal -->
            @include('client.shared.modals.modal')
        </div>
    </div>
</div>