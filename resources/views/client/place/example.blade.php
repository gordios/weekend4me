@extends('layouts.master')
@section('content')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8" id="barba-wrapper">
                    <div class="barba-container">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="hotel" role="tabpanel">
                                <section>
                                    <h3 class="fw-md mb-1 mt-3">6 destinations country found</h3>
                                    <ul class="hotel-title mb-3">
                                        <li class="stars mb-2">
                                            <div class="rating">
                                                <select class="js-rating-stat" data-current-rating="5">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5" selected="selected">5</option>
                                                </select>
                                            </div>
                                        </li>
                                        <li class="d-flex">
                                            <div class="media-object"><i class="icon icon-label mr-2 text-primary"></i></div>
                                            <div class="local"><span>6 countries matched for flight between 05/12/2018 and 07/12/2018</span></div>
                                        </li>
                                    </ul>
                                    <div class="hotel-gallery">
                                        <div class="hotel-gallery__carousel swiper-container js-hotel-carousel">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide"><img class="img-fluid img-cover" src="{{asset('img/flight/upload/1.jpg')}}" alt="#"/>
                                                </div>
                                                <div class="swiper-slide"><img class="img-fluid img-cover" src="{{asset('img/flight/upload/2.jpg')}}" alt="#"/>
                                                </div>
                                                <div class="swiper-slide"><img class="img-fluid img-cover" src="{{asset('img/flight/upload/3.jpg')}}" alt="#"/>
                                                </div>
                                                <div class="swiper-slide"><img class="img-fluid img-cover" src="{{asset('img/flight/upload/4.jpg')}}" alt="#"/>
                                                </div>
                                                <div class="swiper-slide"><img class="img-fluid img-cover" src="{{asset('img/flight/upload/5.jpg')}}" alt="#"/>
                                                </div>
                                                <div class="swiper-slide"><img class="img-fluid img-cover" src="{{asset('img/flight/upload/6.jpg')}}" alt="#"/>
                                                </div>
                                                <div class="swiper-slide"><img class="img-fluid img-cover" src="{{asset('img/flight/upload/7.jpg')}}" alt="#"/>
                                                </div>
                                                <div class="swiper-slide"><img class="img-fluid img-cover" src="{{asset('img/flight/upload/8.jpg')}}" alt="#"/>
                                                </div>
                                                <div class="swiper-slide"><img class="img-fluid img-cover" src="{{asset('img/flight/upload/9.jpg')}}" alt="#"/>
                                                </div>
                                                <div class="swiper-slide"><img class="img-fluid img-cover" src="{{asset('img/flight/upload/10.jpg')}}" alt="#"/>
                                                </div>
                                            </div>
                                            <div class="hotel-gallery__controls"><a class="hotel-gallery__arrow shadow-sm js-prev" role="button"><i class="icon"><svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 44 44"><path d="M22.119 44.237C9.922 44.237 0 34.315 0 22.119S9.922.001 22.119.001s22.119 9.922 22.119 22.118-9.924 22.118-22.119 22.118zm0-42.736C10.75 1.501 1.5 10.75 1.5 22.119c0 11.368 9.25 20.618 20.619 20.618s20.619-9.25 20.619-20.618c0-11.369-9.25-20.618-20.619-20.618z"/><path d="M24.667 29.884a.74.74 0 0 1-.53-.22l-7.328-7.334a.752.752 0 0 1 0-1.061l7.328-7.333a.75.75 0 1 1 1.061 1.061L18.4 21.8l6.798 6.805a.752.752 0 0 1 0 1.061.75.75 0 0 1-.531.218z"/></svg></i></a><a class="hotel-gallery__arrow shadow-sm js-next" role="button"><i class="icon"><svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 44 44"><path d="M22.118 44.236C9.922 44.236 0 34.314 0 22.118S9.922 0 22.118 0s22.118 9.922 22.118 22.118-9.922 22.118-22.118 22.118zm0-42.736C10.75 1.5 1.5 10.749 1.5 22.118c0 11.368 9.25 20.618 20.618 20.618 11.37 0 20.618-9.25 20.618-20.618 0-11.369-9.248-20.618-20.618-20.618z"/><path d="M19.341 29.884a.75.75 0 0 1-.53-1.281l6.796-6.804-6.796-6.803a.75.75 0 1 1 1.061-1.061l7.325 7.333a.75.75 0 0 1 0 1.061l-7.325 7.333a.742.742 0 0 1-.531.222z"/></svg></i></a></div>
                                        </div>
                                        <div class="hotel-gallery__thumbs swiper-container js-hotel-carousel-thumbs">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide"><a class="hotel-gallery__thumb js-gallery-link" href="{{asset('img/flight/upload/1.jpg')}}" data-description="Joshua Tree Homesteader Cabin"><img class="img-cover" src="{{asset('img/flight/upload/1.jpg')}}" alt="#"/></a></div>
                                                <div class="swiper-slide"><a class="hotel-gallery__thumb js-gallery-link" href="{{asset('img/flight/upload/2.jpg')}}" data-description="A perfect place to snuggle up after a fun day in Asheville!"><img class="img-cover" src="{{asset('img/flight/upload/2.jpg')}}" alt="#"/></a></div>
                                                <div class="swiper-slide"><a class="hotel-gallery__thumb js-gallery-link" href="{{asset('img/flight/upload/3.jpg')}}" data-description="A perfect place to snuggle up after a fun day in Asheville!"><img class="img-cover" src="{{asset('img/flight/upload/3.jpg')}}" alt="#"/></a></div>
                                                <div class="swiper-slide"><a class="hotel-gallery__thumb js-gallery-link" href="{{asset('img/flight/upload/4.jpg')}}" data-description="Underfloor heating in the bathroom floor to keep your toes warm!"><img class="img-cover" src="{{asset('img/flight/upload/4.jpg')}}" alt="#"/></a></div>
                                                <div class="swiper-slide"><a class="hotel-gallery__thumb js-gallery-link" href="{{asset('img/flight/upload/5.jpg')}}" data-description="Full kitchen with full size refrigerator and dining table"><img class="img-cover" src="{{asset('img/flight/upload/5.jpg')}}" alt="#"/></a></div>
                                                <div class="swiper-slide"><a class="hotel-gallery__thumb js-gallery-link" href="{{asset('img/flight/upload/6.jpg')}}" data-description="The combination living room and kitchen includes a dining table."><img class="img-cover" src="{{asset('img/flight/upload/6.jpg')}}" alt="#"/></a></div>
                                                <div class="swiper-slide"><a class="hotel-gallery__thumb js-gallery-link" href="{{asset('img/flight/upload/7.jpg')}}" data-description="You'll know you've arrived when you reach the driveway gate."><img class="img-cover" src="{{asset('img/flight/upload/7.jpg')}}" alt="#"/></a></div>
                                                <div class="swiper-slide"><a class="hotel-gallery__thumb js-gallery-link" href="{{asset('img/flight/upload/8.jpg')}}" data-description="Stone walls and steps compliment the garden area behind the brick patio"><img class="img-cover" src="{{asset('img/flight/upload/8.jpg')}}" alt="#"/></a></div>
                                                <div class="swiper-slide"><a class="hotel-gallery__thumb js-gallery-link" href="{{asset('img/flight/upload/9.jpg')}}" data-description="Custom-tiled shower, big enough for two people!"><img class="img-cover" src="{{asset('img/flight/upload/9.jpg')}}" alt="#"/></a></div>
                                                <div class="swiper-slide"><a class="hotel-gallery__thumb js-gallery-link" href="{{asset('img/flight/upload/10.jpg')}}" data-description="View of the backyard parking area as seen from the upstairs unit deck. There is one parking space per unit (one for upstairs, one for downstairs, and one for us in the tiny house in the back!)"><img class="img-cover" src="{{asset('img/flight/upload/10.jpg')}}" alt="#"/></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <form class="hotel-card hotel-status" action="#" method="POST" data-toggle="validator">
                                        <h4>Search parameters</h4>
                                        <div class="row">
                                            <div class="col-12 text-center text-xl-left col-xl">
                                                <div class="row">
                                                    <div class="form-group col-12 col-sm-4">
                                                        <label class="label-text">Depart date</label>
                                                        <div class="input-date-group position-relative"><i class="mr-2 icon icon-calendar text-secondary"></i>
                                                            <input class="form-control js-input-date hidden " id="statusDateFrom" type="text" name="date_from" required="required"/>
                                                            <label class="form-control date" for="statusDateFrom"></label>
                                                        </div><span>flight</span>
                                                    </div>
                                                    <div class="form-group col-12 col-sm-4">
                                                        <label class="label-text">Return date</label>
                                                        <div class="input-date-group position-relative"><i class="mr-2 icon icon-calendar text-secondary"></i>
                                                            <input class="form-control js-input-date hidden " id="statusDateTo" type="text" name="date_to" required="required"/>
                                                            <label class="form-control date" for="statusDateTo"></label>
                                                        </div><span>flight</span>
                                                    </div>
                                                    <div class="form-group col-12 col-sm-4">
                                                        <label class="text-label">Passengers:</label><span class="position-relative">
                                  <input class="form-control" type="hidden" name="persons" value="2 Adults, 1 child"><span class="form-control d-inline pointer">2 Adults, 1 child</span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto mx-auto">
                                                <button class="btn btn-light btn--round" type="button">change search parameters
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="hotel-card hotel-items">
                                        <div class="cart-header">
                                            <h4 class="card-title">Available flights</h4>
                                            <hr>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-8 order-1 order-md-0">
                                                <div class="hotel-package mb-4">
                                                    <div class="hotel-package__row row mb-4">
                                                        <div class="col-6 pr-md-1">
                                                            <a class="hotel-package__img d-block" href="#">
                                                            <img class="img-fluid" src="{{asset('img/flight/hotels/item-7.jpg')}}" alt="#"/></a>
                                                        </div>
                                                        <div class="col-6">
                                                            <h4 class="hotel-package__title d-inline-block">Saint Peterburg</h4>
                                                            <ul class="hotel-package__props">
                                                                <li class="mb-2 rooms">
                                                                    <span class="title mr-1">Flight type:</span><span class="form-select form-select--sm">
                                                                        <select class="form-control select2 js-form-select">
                                                                          <option value="rooms 1">Departure & Return</option>
                                                                        </select>
                                                                    </span>
                                                                </li>
                                                                <li class="mb-2 price">
                                                                    <span class="title mr-2">Price per person:</span>
                                                                    <span class="cost fw-bold">1 286 €</span>
                                                                </li>
                                                                <li class="mb-2 prepayment">
                                                                    <span class="title mr-1">Departure:</span>
                                                                    <span class="fw-bold">05/12/2018</span>
                                                                </li>
                                                                <li class="mb-2 prepayment">
                                                                    <span class="title mr-1">Return:</span>
                                                                    <span class="fw-bold">07/12/2018</span>
                                                                </li>
                                                                <li class="mb-2 guests">
                                                                    <span class="title mr-1">Hotel</span>
                                                                    <span>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                            <button class="btn-more fw-bold text-primary pointer point-fade js-hotel-show-more mb-2" type="button">More info +</button>

                                                            <button class="btn btn-primary btn-sm fw-bold text-white pointer point-fade" type="button">Booking & hotel</button>
                                                        </div>
                                                    </div>
                                                    <div class="collapse js-addition">
                                                        <div class="hotel-package__more">
                                                            <p class="mb-2">A wonderful country to discover</p>
                                                            <ul class="hotel-package__menu">
                                                                <li class="d-flex align-items-center mb-2">
                                                                    <i class="mr-2 icon icon-coffee-cup text-primary"></i>
                                                                    <span class="mt-1 fw-sm">Breakfast is included.</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <hr class="hr-bottom my-0">
                                                </div>
                                                <div class="hotel-package mb-4">
                                                    <div class="hotel-package__row row mb-4">
                                                        <div class="col-6 pr-md-1">
                                                            <a class="hotel-package__img d-block" href="#">
                                                                <img class="img-fluid" src="{{asset('img/flight/hotels/item-8.jpg')}}" alt="#"/></a>
                                                        </div>
                                                        <div class="col-6">
                                                            <h4 class="hotel-package__title d-inline-block">Milan</h4>
                                                            <ul class="hotel-package__props">
                                                                <li class="mb-2 rooms">
                                                                    <span class="title mr-1">Flight type:</span><span class="form-select form-select--sm">
                                                                        <select class="form-control select2 js-form-select">
                                                                          <option value="rooms 1">Departure</option>
                                                                        </select>
                                                                    </span>
                                                                </li>
                                                                <li class="mb-2 price">
                                                                    <span class="title mr-2">Price per person:</span>
                                                                    <span class="cost fw-bold">1 300 €</span>
                                                                </li>
                                                                <li class="mb-2 prepayment">
                                                                    <span class="title mr-1">Departure:</span>
                                                                    <span class="fw-bold">05/12/2018</span>
                                                                </li>
                                                                <li class="mb-2 guests">
                                                                    <span class="title mr-1">Hotel</span>
                                                                    <span>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                            <button class="btn-more fw-bold text-primary pointer point-fade js-hotel-show-more mb-2" type="button">More info +</button>

                                                            <button class="btn btn-primary btn-sm fw-bold text-white pointer point-fade" type="button">Booking & hotel</button>
                                                        </div>
                                                    </div>
                                                    <div class="collapse js-addition">
                                                        <div class="hotel-package__more">
                                                            <p class="mb-2">A wonderful country to discover</p>
                                                            <ul class="hotel-package__menu">
                                                                <li class="d-flex align-items-center mb-2">
                                                                    <i class="mr-2 icon icon-coffee-cup text-primary"></i>
                                                                    <span class="mt-1 fw-sm">Breakfast is included.</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <hr class="hr-bottom my-0">
                                                </div>
                                                <div class="hotel-package mb-4">
                                                    <div class="hotel-package__row row mb-4">
                                                        <div class="col-6 pr-md-1">
                                                            <a class="hotel-package__img d-block" href="#">
                                                                <img class="img-fluid" src="{{asset('img/flight/hotels/item-9.jpg')}}" alt="#"/></a>
                                                        </div>
                                                        <div class="col-6">
                                                            <h4 class="hotel-package__title d-inline-block">Barcelona</h4>
                                                            <ul class="hotel-package__props">
                                                                <li class="mb-2 rooms">
                                                                    <span class="title mr-1">Flight type:</span><span class="form-select form-select--sm">
                                                                        <select class="form-control select2 js-form-select">
                                                                          <option value="rooms 1">Departure & Return</option>
                                                                        </select>
                                                                    </span>
                                                                </li>
                                                                <li class="mb-2 price">
                                                                    <span class="title mr-2">Price per person:</span>
                                                                    <span class="cost fw-bold">1 500 €</span>
                                                                </li>
                                                                <li class="mb-2 prepayment">
                                                                    <span class="title mr-1">Departure:</span>
                                                                    <span class="fw-bold">05/12/2018</span>
                                                                </li>
                                                                <li class="mb-2 prepayment">
                                                                    <span class="title mr-1">Return:</span>
                                                                    <span class="fw-bold">07/12/2018</span>
                                                                </li>
                                                                <li class="mb-2 guests">
                                                                    <span class="title mr-1">Hotel</span>
                                                                    <span>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                            <button class="btn-more fw-bold text-primary pointer point-fade js-hotel-show-more mb-2" type="button">More info +</button>

                                                            <button class="btn btn-primary btn-sm fw-bold text-white pointer point-fade" type="button">Booking & hotel</button>
                                                        </div>
                                                    </div>
                                                    <div class="collapse js-addition">
                                                        <div class="hotel-package__more">
                                                            <p class="mb-2">A wonderful country to discover</p>
                                                            <ul class="hotel-package__menu">
                                                                <li class="d-flex align-items-center mb-2">
                                                                    <i class="mr-2 icon icon-coffee-cup text-primary"></i>
                                                                    <span class="mt-1 fw-sm">Breakfast is included.</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <hr class="hr-bottom my-0">
                                                </div>
                                                <div class="hotel-package mb-4">
                                                    <div class="hotel-package__row row mb-4">
                                                        <div class="col-6 pr-md-1">
                                                            <a class="hotel-package__img d-block" href="#">
                                                                <img class="img-fluid" src="{{asset('img/flight/hotels/item-10.jpg')}}" alt="#"/></a>
                                                        </div>
                                                        <div class="col-6">
                                                            <h4 class="hotel-package__title d-inline-block">Rio de Janeiro</h4>
                                                            <ul class="hotel-package__props">
                                                                <li class="mb-2 rooms">
                                                                    <span class="title mr-1">Flight type:</span><span class="form-select form-select--sm">
                                                                        <select class="form-control select2 js-form-select">
                                                                          <option value="rooms 1">Departure</option>
                                                                        </select>
                                                                    </span>
                                                                </li>
                                                                <li class="mb-2 price">
                                                                    <span class="title mr-2">Price per person:</span>
                                                                    <span class="cost fw-bold">1 700 €</span>
                                                                </li>
                                                                <li class="mb-2 prepayment">
                                                                    <span class="title mr-1">Departure:</span>
                                                                    <span class="fw-bold">05/12/2018</span>
                                                                </li>
                                                                <li class="mb-2 guests">
                                                                    <span class="title mr-1">Hotel</span>
                                                                    <span>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                            <button class="btn-more fw-bold text-primary pointer point-fade js-hotel-show-more mb-2" type="button">More info +</button>

                                                            <button class="btn btn-primary btn-sm fw-bold text-white pointer point-fade" type="button">Booking & hotel</button>
                                                        </div>
                                                    </div>
                                                    <div class="collapse js-addition">
                                                        <div class="hotel-package__more">
                                                            <p class="mb-2">A wonderful country to discover</p>
                                                            <ul class="hotel-package__menu">
                                                                <li class="d-flex align-items-center mb-2">
                                                                    <i class="mr-2 icon icon-coffee-cup text-primary"></i>
                                                                    <span class="mt-1 fw-sm">Breakfast is included.</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <hr class="hr-bottom my-0">
                                                </div>
                                                <div class="hotel-package mb-4">
                                                    <div class="hotel-package__row row mb-4">
                                                        <div class="col-6 pr-md-1">
                                                            <a class="hotel-package__img d-block" href="#">
                                                                <img class="img-fluid" src="{{asset('img/flight/hotels/item-11.jpg')}}" alt="#"/></a>
                                                        </div>
                                                        <div class="col-6">
                                                            <h4 class="hotel-package__title d-inline-block">Paris</h4>
                                                            <ul class="hotel-package__props">
                                                                <li class="mb-2 rooms">
                                                                    <span class="title mr-1">Flight type:</span><span class="form-select form-select--sm">
                                                                        <select class="form-control select2 js-form-select">
                                                                          <option value="rooms 1">Departure & Return</option>
                                                                        </select>
                                                                    </span>
                                                                </li>
                                                                <li class="mb-2 price">
                                                                    <span class="title mr-2">Price per person:</span>
                                                                    <span class="cost fw-bold">1 100 €</span>
                                                                </li>
                                                                <li class="mb-2 prepayment">
                                                                    <span class="title mr-1">Departure:</span>
                                                                    <span class="fw-bold">05/12/2018</span>
                                                                </li>
                                                                <li class="mb-2 prepayment">
                                                                    <span class="title mr-1">Return:</span>
                                                                    <span class="fw-bold">07/12/2018</span>
                                                                </li>
                                                                <li class="mb-2 guests">
                                                                    <span class="title mr-1">Hotel</span>
                                                                    <span>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                            <button class="btn-more fw-bold text-primary pointer point-fade js-hotel-show-more mb-2" type="button">More info +</button>

                                                            <button class="btn btn-primary btn-sm fw-bold text-white pointer point-fade" type="button">Booking & hotel</button>
                                                        </div>
                                                    </div>
                                                    <div class="collapse js-addition">
                                                        <div class="hotel-package__more">
                                                            <p class="mb-2">A wonderful country to discover</p>
                                                            <ul class="hotel-package__menu">
                                                                <li class="d-flex align-items-center mb-2">
                                                                    <i class="mr-2 icon icon-coffee-cup text-primary"></i>
                                                                    <span class="mt-1 fw-sm">Breakfast is included.</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <hr class="hr-bottom my-0">
                                                </div>
                                                <div class="hotel-package mb-4">
                                                    <div class="hotel-package__row row mb-4">
                                                        <div class="col-6 pr-md-1">
                                                            <a class="hotel-package__img d-block" href="#">
                                                                <img class="img-fluid" src="{{asset('img/flight/hotels/item-14.jpg')}}" alt="#"/></a>
                                                        </div>
                                                        <div class="col-6">
                                                            <h4 class="hotel-package__title d-inline-block">Berlin</h4>
                                                            <ul class="hotel-package__props">
                                                                <li class="mb-2 rooms">
                                                                    <span class="title mr-1">Flight type:</span><span class="form-select form-select--sm">
                                                                        <select class="form-control select2 js-form-select">
                                                                          <option value="rooms 1">Departure & Return</option>
                                                                        </select>
                                                                    </span>
                                                                </li>
                                                                <li class="mb-2 price">
                                                                    <span class="title mr-2">Price per person:</span>
                                                                    <span class="cost fw-bold">1 350 €</span>
                                                                </li>
                                                                <li class="mb-2 prepayment">
                                                                    <span class="title mr-1">Departure:</span>
                                                                    <span class="fw-bold">05/12/2018</span>
                                                                </li>
                                                                <li class="mb-2 prepayment">
                                                                    <span class="title mr-1">Return:</span>
                                                                    <span class="fw-bold">08/12/2018</span>
                                                                </li>
                                                                <li class="mb-2 guests">
                                                                    <span class="title mr-1">Hotel</span>
                                                                    <span>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                        <i class="fa fa-star mr-1"></i>
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                            <button class="btn-more fw-bold text-primary pointer point-fade js-hotel-show-more mb-2" type="button">More info +</button>

                                                            <button class="btn btn-primary btn-sm fw-bold text-white pointer point-fade" type="button">Booking & hotel</button>
                                                        </div>
                                                    </div>
                                                    <div class="collapse js-addition">
                                                        <div class="hotel-package__more">
                                                            <p class="mb-2">A wonderful country to discover</p>
                                                            <ul class="hotel-package__menu">
                                                                <li class="d-flex align-items-center mb-2">
                                                                    <i class="mr-2 icon icon-coffee-cup text-primary"></i>
                                                                    <span class="mt-1 fw-sm">Breakfast is included.</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <hr class="hr-bottom my-0">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-4 hotel-items__check">
                                                <div class="py-3 js-sticky-top">
                                                    <p class="mb-4">Flight with us.</p>
                                                    <p class="mb-2"><a class="btn btn-secondary btn--round btn-order" href="{{route('client.dashboard.index')}}" role="button">Welcome</a>
                                                    </p>
                                                    <p>Discover these amazing countries with us.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
                <aside class="sidebar col-lg-4">
                    <div class="sidebar__content js-sticky-top" style="width:380px;">
                        <div class="sidebar__finder card bg-primary text-white">
                            <h4 class="m-0 mb-lg-4">Search on Weekend4Me</h4>
                            <form class="collapse show d-lg-block mt-3" id="hotelFinder" action="#" method="POST" data-toggle="validator">
                                <div class="row">
                                    <div class="col-1 form-group">
                                        <i class="fa fa-plane" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-9 form-group">
                                        <h4 class="m-0 mb-lg-4">Check our pending flights</h4>
                                    </div>

                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-12 col-sm-6 col-lg-12 form-group">
                                                <label class="label-text" for="hotelInDate">Departure date</label>
                                                <div class="input-group-append"><i class="icon icon-calendar"></i>
                                                    <input class="form-control js-input-date " id="hotelInDate" type="text" name="hotel_date_in" required="required"/>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-lg-12 form-group">
                                                <label class="label-text" for="hotelOutDate">Return date</label>
                                                <div class="input-group-append"><i class="icon icon-calendar"></i>
                                                    <input class="form-control js-input-date " id="hotelOutDate" type="text" name="hotel_date_out" required="required"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <!-- <label class="label">13 Nights</label> -->
                                        <div class="row">
                                            <div class="col-12 col-sm-6 form-group col-lg-12">
                                                <span class="form-select">
                                                  <select class="form-control select2 js-form-select" id="hotelAdults" name="hotel_adults">
                                                    <option value="1 Adult">1 Adult</option>
                                                    <option value="2 Adults">2 Adults</option>
                                                    <option value="3 Adults">3 Adults</option>
                                                    <option value="4 Adults">4 Adults</option>
                                                    <option value="5 Adults">5 Adults</option>
                                                  </select>
                                                </span>
                                            </div>
                                            <div class="col-12 col-sm-6 form-group col-lg-6"><span class="form-select">
                                                  <select class="form-control select2 js-form-select" id="hotelChild" name="hotel__childs">
                                                    <option value="1 Child">1 Child</option>
                                                    <option value="2 Childs">2 Childs</option>
                                                    <option value="3 Childs">3 Childs</option>
                                                    <option value="4 Childs">4 Childs</option>
                                                    <option value="5 Childs">5 Childs</option>
                                                  </select>
                                                </span>
                                            </div>
                                            <div class="col-12 col-sm-6 form-group col-lg-6"><span class="form-select">
                                                  <select class="form-control select2 js-form-select" id="hotelRoom" name="hotel_rooms">
                                                    <option value="1 Room">1 Room</option>
                                                    <option value="2 Rooms">2 Rooms</option>
                                                    <option value="3 Rooms">3 Rooms</option>
                                                    <option value="4 Rooms">4 Rooms</option>
                                                    <option value="5 Rooms">5 Rooms</option>
                                                  </select>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-secondary btn--round mx-auto mt-2 w-100" type="button">search
                                </button>
                            </form>
                        </div>
                        <button class="btn-toggle btn btn-primary-light border-0 w-100 py-3 d-lg-none" data-target="#hotelFinder" data-toggle="collapse" aria-expanded="true" type="button"><span class="icon-bar"></span></button>
                    </div>
                </aside>

            </div>
        </div>
        <button class="btn btn-primary btn-nav btn-nav--up js-scroll-up" type="button"><i class="fa fa-angle-up"></i></button>
    </div>
@endsection
