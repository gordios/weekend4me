@extends('layouts.master')
@section('content')
<div class="page-content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6">
        <div class="left" style="margin-left: -326px;height: 600px;width:1000px;background-color:white;">
          <div class="blue">
          </div>
          <img class="img-fluid mb-md-1" src="/flight/img/map.png" alt="#" style="margin-top: -132px;"/>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="right" style="background-color:#75b918;height:600px;width:1000px;">
      </div>
    </div>
  </div>
  <button class="btn btn-primary btn-nav btn-nav--up js-scroll-up" type="button"><i class="fa fa-angle-up"></i></button>
</div>
@endsection
