@extends('layouts.master')
@section('content')
  <section class="intro d-flex flex-column load">
    <div class="intro__bg js-intro-bg">
      <div class="over"></div>
      <div class="swiper-container js-intro-slider-bg">
        <div class="swiper-wrapper">
          <div class="swiper-slide">
              <img class="img-cover" src="{{asset('uploads/images/uibody/item-3.jpg')}}" alt="#">
          </div>
        </div>
      </div>
    </div>

    <div class="intro__content d-flex flex-column justify-content-end js-intro-content">
      <div class="row text-center text-white">
        <div class="col-12 col-lg-10 mx-auto">
          <h4 class="h4 intro__caption prlx-scroll">City tours / tour tickets / tour guides</h4>
          <h1 class="h1 intro__title prlx-scroll">more tours</h1>
          <p class="prlx-scroll">
            city tours
            <span class="js-counter">4217</span>
            accommodation options - to see them, just enter the dates!
          </p>
        </div>
      </div>
      @include('client.shared.searchbar')

      <div class="intro__hotels">
        <div class="container-fluid p-0">
          <div class="swiper-container js-intro-hotels">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <div class="card-intro d-block card-intro-1">
                  <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
                  <div class="card-intro__footer">
                    <h4 class="h4 f-primary">Budapest</h4>
                    <div class="card-intro__local d-flex align-items-center"><i class="icon icon-label mr-1"></i><span>Thailand</span></div>
                    <div class="card-intro__rating">
                      <select class="js-rating-stat" data-current-rating="5">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5" selected="selected">5</option>
                      </select>
                    </div>
                  </div>
                  <div class="card-hover">
                    <h3 class="h3 text-uppercase">Budapest</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="card-intro d-block card-intro-2">
                  <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
                  <div class="card-intro__footer">
                    <h4 class="h4 f-primary">Berlin</h4>
                    <div class="card-intro__local d-flex align-items-center"><i class="icon icon-label mr-1"></i><span>Berlin</span></div>
                    <div class="card-intro__rating">
                      <select class="js-rating-stat" data-current-rating="4">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4" selected="selected">4</option>
                        <option value="5">5</option>
                      </select>
                    </div>
                  </div>
                  <div class="card-hover">
                    <h3 class="h3 text-uppercase">Majorca</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="card-intro d-blpek card-intro-3">
                  <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
                  <div class="card-intro__footer">
                    <h4 class="h4 f-primary">Majorca</h4>
                    <div class="card-intro__local d-flex align-items-center"><i class="icon icon-label mr-1"></i><span>Costa Brava, Spain</span></div>
                    <div class="card-intro__rating">
                      <select class="js-rating-stat" data-current-rating="4">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4" selected="selected">4</option>
                        <option value="5">5</option>
                      </select>
                    </div>
                  </div>
                  <div class="card-hover">
                    <h3 class="h3 text-uppercase">Marsol</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="card-intro d-block card-intro-4">
                  <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
                  <div class="card-intro__footer">
                    <h4 class="h4 f-primary">Macronissos Village Bungalows</h4>
                    <div class="card-intro__local d-flex align-items-center"><i class="icon icon-label mr-1"></i><span>Ayia Napa, Cyprus</span></div>
                    <div class="card-intro__rating">
                      <select class="js-rating-stat" data-current-rating="4">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4" selected="selected">4</option>
                        <option value="5">5</option>
                      </select>
                    </div>
                  </div>
                  <div class="card-hover">
                    <h3 class="h3 text-uppercase">Macronissos Village Bungalows</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="card-intro d-block card-intro-5">
                  <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
                  <div class="card-intro__footer">
                    <h4 class="h4 f-primary">Berlin</h4>
                    <div class="card-intro__local d-flex align-items-center"><i class="icon icon-label mr-1"></i><span>Hurghada, Egypt</span></div>
                    <div class="card-intro__rating">
                      <select class="js-rating-stat" data-current-rating="4">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4" selected="selected">4</option>
                        <option value="5">5</option>
                      </select>
                    </div>
                  </div>
                  <div class="card-hover">
                    <h3 class="h3 text-uppercase">Geneva</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="intro__hotels-controls">
            <button class="btn btn-primary btn-nav btn-nav--left js-prev" type="button"><i class="fa fa-angle-left"></i></button>
            <button class="btn btn-primary btn-nav btn-nav--right js-next" type="button"><i class="fa fa-angle-right"></i></button>
          </div>
        </div>
      </div>

    </div>
  </section>

  <div class="page-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 text-center">
          <div class="section-header">
            <h2 class="h2">Popular destinations around the world</h2>
            <div class="section-header__stars mb-3"><i class="fa fa-star"></i><i class="fa fa-star center"></i><i class="fa fa-star"></i></div>
            <p class="fz-norm mb-0"><em>The best choice of hotels we have</em></p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-hotel w-100 mb-2">
            <div class="card-hotel__img"><img class="img-cover" src="{{asset('img/flight/hotels/item-7.jpg')}}" alt="#"/>
            </div>
            <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
            <div class="card-hotel__bottom">
              <h4 class="h4 mb-1">Saint Peterburg</h4>
              <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Hotels</li>
                  <li class="amout">
                  </li>
                  <select class="js-rating-stat" data-current-rating="4">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4" selected="selected">4</option>
                    <option value="5">5</option>
                  </select>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Departure</li>
                  <li class="amout">
                    Fri 18/01/19 at 7:25
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Return</li>
                  <li class="amout">
                    Sun 21/01/19 at 7:25
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-hover">
              <h3 class="h3 text-uppercase">Saint Peterburg</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-hotel w-100 mb-2">
            <div class="card-hotel__img"><img class="img-cover" src="{{asset('img/flight/hotels/item-8.jpg')}}" alt="#"/>
            </div>
            <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
            <div class="card-hotel__bottom">
              <h4 class="h4 mb-1">Milan</h4>
              <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Italy</li>
                  <select class="js-rating-stat" data-current-rating="4">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4" selected="selected">4</option>
                    <option value="5">5</option>
                  </select>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Departure</li>
                  <li class="amout">
                    Fri 18/01/19 at 7:25
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Return</li>
                  <li class="amout">
                    Sun 21/01/19 at 7:25
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-hover">
              <h3 class="h3 text-uppercase">Milan</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-hotel w-100 mb-2">
            <div class="card-hotel__img"><img class="img-cover" src="{{asset('img/flight/hotels/item-9.jpg')}}" alt="#"/>
            </div>
            <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
            <div class="card-hotel__bottom">
              <h4 class="h4 mb-1">Barselona</h4>
              <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Spain</li>
                  <li class="amout">
                    <select class="js-rating-stat" data-current-rating="4">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4" selected="selected">4</option>
                      <option value="5">5</option>
                    </select>
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Departure</li>
                  <li class="amout">
                    Fri 18/01/19 at 7:25
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Return</li>
                  <li class="amout">
                    Sun 21/01/19 at 7:25
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-hover">
              <h3 class="h3 text-uppercase">Barselona</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-hotel w-100 mb-2">
            <div class="card-hotel__img"><img class="img-cover" src="{{asset('img/flight/hotels/item-10.jpg')}}" alt="#"/>
            </div>
            <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
            <div class="card-hotel__bottom">
              <h4 class="h4 mb-1">Rio de Janeiro</h4>
              <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Brazil</li>
                  <li class="amout">
                    <select class="js-rating-stat" data-current-rating="4">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4" selected="selected">4</option>
                      <option value="5">5</option>
                    </select>
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Departure</li>
                  <li class="amout">
                    Fri 18/01/19 at 7:25
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Return</li>
                  <li class="amout">
                    Sun 21/01/19 at 7:25
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Departure</li>
                <li class="amout">
                  Fri 18/01/19 at 7:25
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Return</li>
                <li class="amout">
                  Sun 21/01/19 at 7:25
                </li>
              </ul>
            </div>
            <div class="card-hover">
              <h3 class="h3 text-uppercase">Rio de Janeiro</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-hotel w-100 mb-2">
            <div class="card-hotel__img"><img class="img-cover" src="{{asset('img/flight/hotels/item-11.jpg')}}" alt="#"/>
            </div>
            <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
            <div class="card-hotel__bottom">
              <h4 class="h4 mb-1">Paris</h4>
              <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">England</li>
                  <li class="amout">
                    <select class="js-rating-stat" data-current-rating="4">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4" selected="selected">4</option>
                      <option value="5">5</option>
                    </select>
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Departure</li>
                  <li class="amout">
                    Fri 18/01/19 at 7:25
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Return</li>
                  <li class="amout">
                    Sun 21/01/19 at 7:25
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-hover">
              <h3 class="h3 text-uppercase">Paris</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-hotel w-100 mb-2">
            <div class="card-hotel__img"><img class="img-cover" src="{{asset('img/flight/hotels/item-12.jpg')}}" alt="#"/>
            </div>
            <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
            <div class="card-hotel__bottom">
              <h4 class="h4 mb-1">Barselona</h4>
              <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">France</li>
                  <li class="amout">
                    <select class="js-rating-stat" data-current-rating="4">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4" selected="selected">4</option>
                      <option value="5">5</option>
                    </select>
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Departure</li>
                  <li class="amout">
                    Fri 18/01/19 at 7:25
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Return</li>
                  <li class="amout">
                    Sun 21/01/19 at 7:25
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-hover">
              <h3 class="h3 text-uppercase">Barselona</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-hotel w-100 mb-2">
            <div class="card-hotel__img"><img class="img-cover" src="{{asset('img/flight/hotels/item-13.jpg')}}" alt="#"/>
            </div>
            <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
            <div class="card-hotel__bottom">
              <h4 class="h4 mb-1">Amsterdam</h4>
              <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Netherlands</li>
                  <li class="amout">
                    <select class="js-rating-stat" data-current-rating="4">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4" selected="selected">4</option>
                      <option value="5">5</option>
                    </select>
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Departure</li>
                  <li class="amout">
                    Fri 18/01/19 at 7:25
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Return</li>
                  <li class="amout">
                    Sun 21/01/19 at 7:25
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-hover">
              <h3 class="h3 text-uppercase">Amsterdam</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-hotel w-100 mb-2">
            <div class="card-hotel__img"><img class="img-cover" src="{{asset('img/flight/hotels/item-14.jpg')}}" alt="#"/>
            </div>
            <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
            <div class="card-hotel__bottom">
              <h4 class="h4 mb-1">Berlin</h4>
              <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Germany</li>
                  <li class="amout">
                    <select class="js-rating-stat" data-current-rating="4">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4" selected="selected">4</option>
                      <option value="5">5</option>
                    </select>
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Departure</li>
                  <li class="amout">
                    Fri 18/01/19 at 7:25
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Return</li>
                  <li class="amout">
                    Sun 21/01/19 at 7:25
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-hover">
              <h3 class="h3 text-uppercase">Berlin</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-hotel w-100 mb-2">
            <div class="card-hotel__img"><img class="img-cover" src="{{asset('img/flight/hotels/item-15.jpg')}}" alt="#"/>
            </div>
            <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
            <div class="card-hotel__bottom">
              <h4 class="h4 mb-1">Budapest</h4>
              <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Hungary</li>
                  <li class="amout">
                    <select class="js-rating-stat" data-current-rating="4">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4" selected="selected">4</option>
                      <option value="5">5</option>
                    </select>
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Departure</li>
                  <li class="amout">
                    Fri 18/01/19 at 7:25
                  </li>
                </ul>
              </div>
              <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
                <ul class="d-inline-flex flex-wrap">
                  <li class="mr-4">Return</li>
                  <li class="amout">
                    Sun 21/01/19 at 7:25
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-hover">
              <h3 class="h3 text-uppercase">Budapest</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
            </div>
          </div>
        </div>
        <div class="col-12 page-section__more text-center">
          <button class="btn btn-secondary btn--round btn-load" type="button">Show more<i class="fa fa-spin"></i>
          </button>
        </div>
      </div>
      <div class="row">
        <div class="col-12 text-center">
          <div class="section-header">
            <h2 class="h2">The opportunities we provide</h2>
            <div class="section-header__stars mb-3"><i class="fa fa-star"></i><i class="fa fa-star center"></i><i class="fa fa-star"></i></div>
            <p class="fz-norm mb-0"><em>Explore the features and benefits of our service</em></p>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-service w-100 mb-2">
            <div class="card-header">
              <h4 class="h4">Handpicked Hotels</h4>
            </div>
            <div class="card-body">
              <p>All hotels on our website are tested according to various criteria. You can be sure of your choice.</p>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-service w-100 mb-2">
            <div class="card-header">
              <h4 class="h4">Detailed Descriptions</h4>
            </div>
            <div class="card-body">
              <p>In order for you to have the most complete idea about the hotel, we try to collect the most complete and detailed description.</p>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-service w-100 mb-2">
            <div class="card-header">
              <h4 class="h4">Best Price Guarantee</h4>
            </div>
            <div class="card-body">
              <p>We offer the best hotels at the best prices. If you find the same room category on the same dates cheaper elsewhere, we will refund the difference. </p>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-service w-100 mb-2">
            <div class="card-header">
              <h4 class="h4">Secure Booking</h4>
            </div>
            <div class="card-body">
              <p>Book hotels with us easily and safely. All data on your credit card is encrypted and secure. You can be calm for your means.</p>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-service w-100 mb-2">
            <div class="card-header">
              <h4 class="h4">Better service</h4>
            </div>
            <div class="card-body">
              <p>Our specialists visit various hotels to personally assess their quality and provide you with a detailed review.</p>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
          <div class="card card-service w-100 mb-2">
            <div class="card-header">
              <h4 class="h4">Any Questions?{{__('language.welcome')}}</h4>
            </div>
            <div class="card-body">
              <p>Call us at 8-800-2000-6000 and we will answer your questions, help you find a hotel and make a reservation. Working 24/7</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <button class="btn btn-primary btn-nav btn-nav--up js-scroll-up" type="button"><i class="fa fa-angle-up"></i></button>
  </div>
@endsection
