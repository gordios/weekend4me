@extends('layouts.master')
@section('content')
<div class="page-content">
  <div class="container">
    <div class="row">
      <aside class="sidebar col-lg-3 order-1 order-lg-0">
        <div class="sidebar__content js-sticky-top">
          <section class="sidebar__card card">
            <div class="sidebar__card-title">
              <h4 class="card-title">Need Help Booking?</h4>
              <hr class="my-3"/>
            </div>
            <p>Call our customer services team on the number below to speak to one of our advisors who will help you with all of your holiday needs.</p>
            <ul class="sidebar__contacts">
              <li class="d-flex align-items-center"><i class="icon icon-phone mr-2"></i><a class="fw-bold" href="tel:#">8-800-0011-2222-333</a></li>
            </ul>
          </section>
          <section class="sidebar__card card">
            <div class="sidebar__card-title">
              <h4 class="card-title">Category</h4>
              <hr class="my-3"/>
            </div>
            <ul class="sidebar__list sidebar__category">
              <li><a href="#"><span>Cities of the world</span><span class="count"> (15)</span></a></li>
              <li><a href="#"><span>Interesting places</span><span class="count"> (22)</span></a></li>
              <li><a href="#"><span>Cultural tradition</span><span class="count"> (5)</span></a></li>
              <li><a href="#"><span>Sights</span><span class="count"> (27)</span></a></li>
              <li><a href="#"><span>Budget travel</span><span class="count"> (17)</span></a></li>
              <li><a href="#"><span>Articles from tourists</span><span class="count"> (74)</span></a></li>
            </ul>
          </section>
          <section class="sidebar__card card">
            <div class="sidebar__card-title">
              <h4 class="card-title">Our advantages</h4>
              <hr class="my-3"/>
            </div>
            <ul class="sidebar__list sidebar__adventages">
              <li>
                <h5 class="title mb-2 text-primary"><a class="point-under">Huge selection</a></h5>
                <p>Call our customer services team on the number below to speak to one of our advisors who will help you with all of your holiday needs.</p>
              </li>
              <li>
                <h5 class="title mb-2 text-primary"><a class="point-under">Low prices</a></h5>
                <p>Call our customer services team on the number below to speak to one of our advisors who will help you with all of your holiday needs.</p>
              </li>
              <li>
                <h5 class="title mb-2 text-primary"><a class="point-under">Booking without commission</a></h5>
                <p>Call our customer services team on the number below to speak to one of our advisors who will help you with all of your holiday needs.</p>
              </li>
            </ul>
          </section>
          <section class="sidebar__card card">
            <div class="sidebar__card-title">
              <h4 class="card-title">Archive</h4>
              <hr class="my-3"/>
            </div>
            <ul class="sidebar__list sidebar__archive">
              <li><a href="#">January 2018</a></li>
              <li><a href="#">December 2017</a></li>
              <li><a href="#">November 2017</a></li>
              <li><a href="#">Ocotober 2017</a></li>
              <li><a href="#">September 2017</a></li>
              <li><a href="#">August 2017</a></li>
            </ul>
          </section>
        </div>
      </aside>
      <div class="col-lg-9 blog-content">
        <div class="row">
          <div class="blog-list-item col-12"><a class="blog-post-wrap" href="blog-post.html">
              <article class="blog-post card">
                <div class="blog-post__head mb-3">
                  <h2 class="blog-post__title mb-3">Features holiday on the French Riviera
                  </h2>
                  <ul class="blog-post__meta">
                    <li class="date"><i class="icon icon-calendar"></i>
                      <time datetime="2017-01-18">18.01.2017</time>
                    </li>
                    <li class="status"><i class="icon icon-user"></i><span>Admin</span></li>
                    <li><i class="icon icon-tags"></i>
                      <ol class="blog-post__meta-tags">
                        <li><span>France</span>
                        </li>
                        <li><span>Nice</span>
                        </li>
                        <li><span>beach holidays</span>
                        </li>
                      </ol>
                    </li>
                    <li><i class="icon icon-comments"></i><span>5</span>
                    </li>
                  </ul>
                </div>
                <figure class="blog-post__cover mb-3 border-bottom"><span class="d-block img-wrap"><img class="img-cover" src="/flight/img/blog/1.jpg" alt="Features holiday on the French Riviera"/></span></figure>
                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p>
                <div class="blog-post__more"><span class="blog-post__more-link text-primary"><span class="mr-2">Read more</span><i class="icon icon-arrow-right"></i></span></div>
              </article></a></div>
          <div class="blog-list-item col-12"><a class="blog-post-wrap" href="blog-post.html">
              <article class="blog-post card">
                <div class="blog-post__head mb-3">
                  <h2 class="blog-post__title mb-3">What is important to know when traveling in Europe
                  </h2>
                  <ul class="blog-post__meta">
                    <li class="date"><i class="icon icon-calendar"></i>
                      <time datetime="2017-01-18">18.01.2017</time>
                    </li>
                    <li class="status"><i class="icon icon-user"></i><span>Admin</span></li>
                    <li><i class="icon icon-tags"></i>
                      <ol class="blog-post__meta-tags">
                        <li><span>France</span>
                        </li>
                        <li><span>Nice</span>
                        </li>
                        <li><span>beach holidays</span>
                        </li>
                      </ol>
                    </li>
                    <li><i class="icon icon-comments"></i><span>5</span>
                    </li>
                  </ul>
                </div>
                <figure class="blog-post__cover mb-3 border-bottom"><span class="d-block img-wrap"><img class="img-cover" src="/flight/img/blog/2.jpg" alt="What is important to know when traveling in Europe"/></span></figure>
                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p>
                <div class="blog-post__more"><span class="blog-post__more-link text-primary"><span class="mr-2">Read more</span><i class="icon icon-arrow-right"></i></span></div>
              </article></a></div>
          <div class="blog-list-item col-12"><a class="blog-post-wrap" href="blog-post.html">
              <article class="blog-post card">
                <div class="blog-post__head mb-3">
                  <h2 class="blog-post__title mb-3">What to do in Santorini
                  </h2>
                  <ul class="blog-post__meta">
                    <li class="date"><i class="icon icon-calendar"></i>
                      <time datetime="2017-01-18">18.01.2017</time>
                    </li>
                    <li class="status"><i class="icon icon-user"></i><span>Admin</span></li>
                    <li><i class="icon icon-tags"></i>
                      <ol class="blog-post__meta-tags">
                        <li><span>France</span>
                        </li>
                        <li><span>Nice</span>
                        </li>
                        <li><span>beach holidays</span>
                        </li>
                      </ol>
                    </li>
                    <li><i class="icon icon-comments"></i><span>5</span>
                    </li>
                  </ul>
                </div>
                <figure class="blog-post__cover mb-3 border-bottom"><span class="d-block img-wrap"><img class="img-cover" src="/flight/img/blog/3.jpg" alt="What to do in Santorini"/></span></figure>
                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p>
                <div class="blog-post__more"><span class="blog-post__more-link text-primary"><span class="mr-2">Read more</span><i class="icon icon-arrow-right"></i></span></div>
              </article></a></div>
          <div class="blog-list-item col-12"><a class="blog-post-wrap" href="blog-post.html">
              <article class="blog-post card">
                <div class="blog-post__head mb-3">
                  <h2 class="blog-post__title mb-3">Where is the best beach holiday
                  </h2>
                  <ul class="blog-post__meta">
                    <li class="date"><i class="icon icon-calendar"></i>
                      <time datetime="2017-01-18">18.01.2017</time>
                    </li>
                    <li class="status"><i class="icon icon-user"></i><span>Admin</span></li>
                    <li><i class="icon icon-tags"></i>
                      <ol class="blog-post__meta-tags">
                        <li><span>France</span>
                        </li>
                        <li><span>Nice</span>
                        </li>
                        <li><span>beach holidays</span>
                        </li>
                      </ol>
                    </li>
                    <li><i class="icon icon-comments"></i><span>5</span>
                    </li>
                  </ul>
                </div>
                <figure class="blog-post__cover mb-3 border-bottom"><span class="d-block img-wrap"><img class="img-cover" src="/flight/img/blog/4.jpg" alt="Where is the best beach holiday"/></span></figure>
                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p>
                <div class="blog-post__more"><span class="blog-post__more-link text-primary"><span class="mr-2">Read more</span><i class="icon icon-arrow-right"></i></span></div>
              </article></a></div>
          <div class="blog-list-item col-12"><a class="blog-post-wrap" href="blog-post.html">
              <article class="blog-post card">
                <div class="blog-post__head mb-3">
                  <h2 class="blog-post__title mb-3">How to save a tourist on air tickets
                  </h2>
                  <ul class="blog-post__meta">
                    <li class="date"><i class="icon icon-calendar"></i>
                      <time datetime="2017-01-18">18.01.2017</time>
                    </li>
                    <li class="status"><i class="icon icon-user"></i><span>Admin</span></li>
                    <li><i class="icon icon-tags"></i>
                      <ol class="blog-post__meta-tags">
                        <li><span>France</span>
                        </li>
                        <li><span>Nice</span>
                        </li>
                        <li><span>beach holidays</span>
                        </li>
                      </ol>
                    </li>
                    <li><i class="icon icon-comments"></i><span>5</span>
                    </li>
                  </ul>
                </div>
                <figure class="blog-post__cover mb-3 border-bottom"><span class="d-block img-wrap"><img class="img-cover" src="/flight/img/blog/5.jpg" alt="How to save a tourist on air tickets"/></span></figure>
                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p>
                <div class="blog-post__more"><span class="blog-post__more-link text-primary"><span class="mr-2">Read more</span><i class="icon icon-arrow-right"></i></span></div>
              </article></a></div>
        </div>
        <div class="mb-4 mt-lg-2 mb-lg-0">
          <nav class="pagination-nav" aria-label="Page navigation example">
            <ul class="pagination">
              <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-double-left"></i></a></li>
              <li class="page-item"><a class="page-link" href="#">1</a>
              </li>
              <li class="page-item"><a class="page-link" href="#">2</a>
              </li>
              <li class="page-item align-self-end d-sm-none"><a class="dotted" href="#">......</a>
              </li>
              <li class="page-item d-none d-sm-block"><a class="page-link" href="#">4</a>
              </li>
              <li class="page-item d-none d-sm-block"><a class="page-link" href="#">5</a>
              </li>
              <li class="page-item d-none d-sm-block"><a class="page-link" href="#">6</a>
              </li>
              <li class="page-item"><a class="page-link" href="#">7</a>
              </li>
              <li class="page-item"><a class="page-link" href="#">8</a>
              </li>
              <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-double-right"></i></a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <button class="btn btn-primary btn-nav btn-nav--up js-scroll-up" type="button"><i class="fa fa-angle-up"></i></button>
</div>
@endsection
